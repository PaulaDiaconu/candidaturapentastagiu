package pentastagiu.java.implementation;

import java.util.List;

/***
 * 
 * @author Paula Diaconu
 * 
 * Given a series of numbers 1!..n! find and print the longest sequence 
 * of numbers where the digit sum for their factorials does not increase.
 * digitSum(x!) <= digitSum((x+1)!) .. <= digitSum((x+k))
 * Prints: x x+1 .. x+k
 *
 */
public class Main {

	public static void main(String[] args) {
		
		SequenceCalculator calc = new SequenceCalculator();
		
		List<Integer> list = calc.generateLongestSequenceOfNumbers(1, 10);
		
		for(int i : list){
			System.out.print(i + " ");
		}
	}

}
