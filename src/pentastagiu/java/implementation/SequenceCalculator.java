package pentastagiu.java.implementation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import pentastagiu.java.utils.MathUtils;


/***
 * 
 * @author Paula Diaconu
 * 
 * This class is used to find the longest sequence of numbers 
 * within a interval where the digit sum for their factorials does not increase.
 *
 */
public class SequenceCalculator {
	
	private ArrayList<Integer> digitSumFactorials; 

	public SequenceCalculator()
	{
		digitSumFactorials = new ArrayList<Integer>();
	}
	
	private void computeFactorialsSums(int beginingOfInterval, int endOfInterval)
	{
		int current = beginingOfInterval;
		long currentFactorial = MathUtils.factorial(current);
		int digitSum = MathUtils.digitSumOfNumber(currentFactorial);
		digitSumFactorials.add(digitSum);
		
		for(current = current + 1; current <= endOfInterval; current++)
		{
			currentFactorial *= current;
			digitSum = MathUtils.digitSumOfNumber(currentFactorial);
			digitSumFactorials.add(digitSum);
		}
	}
	
	/***
	 * 
	 * @param beginingOfInterval is the first number in the given interval
	 * @param endOfInterval is the last number in the given interval
	 * @return longestSequence which is a list with the consecutive numbers that 
	 * 		   the digit sum for their factorials does not increase.
	 */
	public List<Integer> generateLongestSequenceOfNumbers(int beginingOfInterval, int endOfInterval)
	{
		int previous;
		int current;
		int currentSum = 1;
		int maxSum = 1;
		int startedIndex = 0;
		List<Integer> longestSequence = new LinkedList<Integer>();
		
		digitSumFactorials.clear();
		computeFactorialsSums(beginingOfInterval, endOfInterval);
		current = digitSumFactorials.get(0);
		
		for(int i = 1; i < digitSumFactorials.size(); i++){
			previous = current;
			current = digitSumFactorials.get(i);
			
			if(current <= previous) {
				currentSum = currentSum + 1;
			} else {
				if(maxSum < currentSum) {
					maxSum = currentSum;
					startedIndex = i - maxSum;
				}
				currentSum = 1;
			}
		}
		
		for(int i = startedIndex; i < maxSum + startedIndex; i++) {
			longestSequence.add(i + beginingOfInterval);
		}
		
		return longestSequence;
	}
}
