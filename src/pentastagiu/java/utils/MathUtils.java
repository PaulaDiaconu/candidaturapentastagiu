package pentastagiu.java.utils;

/***
 * 
 * @author Paula Diaconu
 * 
 * This class is used to make operations with numbers
 *
 */
public class MathUtils {
	
	/***
	 * This method calculates the sum of the digits of a number given as a parameter
	 * @param number
	 * @return
	 */
	public static int digitSumOfNumber(long number)
	{
		int sum = 0;
		while(number != 0)
		{
			sum += number%10;
			number = number/10;
		}
		return sum;
	}
	
	/***
	 * This method calculates the factorial of a number given as a parameter
	 * @param number
	 * @return
	 */
	public static long factorial(int number)
	{
		int i;
		long p = 1;
		if (number == 0)
			return 1;
		else {
			for (i = 1; i <= number; i++)
				p *= i;
			return p;
		}
	}

}
